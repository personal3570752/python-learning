class Mammal:
    def walk(self):
        print("walk")


class Dog(Mammal):  # Inheriting 'Mammal' class
    def bark(self):
        print("bark")


class Cat(Mammal):
    def be_annoying(self):
        print("annoying")


dog = Dog()
dog.bark()

cat = Cat()
cat.walk()
cat.be_annoying()
