import random as r


class Dice:
    def roll(self):
        first = r.randint(1, 6)
        second = r.randint(1, 6)
        return first, second  # returns tuple


dice = Dice()
print(dice.roll())
