from pathlib import Path

# Absolute Path => c:\Program Files\Microsoft
# Relative Path => starting from our current directory

# p = Path("emails")
# print(p.mkdir())
# print(p.rmdir())  # remove the directory
# print(p.exists())

path = Path()  # current directory
for file in path.glob("*"):
    print(file)