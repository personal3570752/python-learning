def greet_user(first_name, last_name):
    print(f'Hi {first_name} {last_name}!')
    print('Welcome aboard ')


def square(number):
    return number ** 2


# print("Start")
# greet_user('John', 'Smith')
# greet_user(last_name='Smith', first_name='John')
# print("Finish")


print(square(4))