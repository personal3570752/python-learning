is_hot = False
is_cold = True

# if is_hot:
#     print("It is a hot day")
# elif is_cold:
#     print("It is a cold day")
# else:
#     print("It is a lovely day")
#
# print('Enjoy your day')


# WHILE LOOP
# i = 1
# while i <= 5:
#     print('*' * i)
#     i = i + 1
# print('Done')


# FOR LOOPS

# for item in 'Python':
#     print(item)

# for item in ['Mosh', 'John', 'Sarah']:
#     print(item)

for item in range(1, 10, 2):  # from 1 to 10(exclude) by incrementing 2 in each iteration
    print(item)
