# MATRIX

# matrix = [
#     [1, 2, 3],
#     [4, 5, 6],
#     [7, 8, 9]
# ]
# print(matrix)


# LISTS

# names = ['John', 'Bob', 'Mosh', 'Sarah', 'Mary']
# print(names[0])
# print(names[2])
# print(names[-1]) # last item in the list
# print(names[1:4])

# numbers = [5, 2, 1, 7, 5, 4]
# numbers.append(20)
# numbers.insert(2, 10)
# numbers.remove(5) # throw error if the item is not in the list
# numbers.clear()
# numbers.pop() # remove the last element
# print(numbers.index(1)) # throw error if the item is not in the list
# print(50 in numbers)
# print(numbers.count(5)) # count the occurrence of the item
# numbers.sort()
# numbers.reverse()
# numbers2 = numbers.copy()
# print(numbers)


# TUPLES
# numbers = (1, 2, 3)
# print(numbers[1])

# coordinates = (2, 5, 8)
# x, y, z = coordinates
# print(x)
# print(y)
# print(z)


# DICTIONARIES
# customer = {
#     "name": "John Smith",
#     "age": 30,
#     "is_verified": True
# }
#
# customer["name"] = "Jack Smith"
# customer["birthdate"] = "Jan 1 1980" # adding new key to dictionary
# print(customer["name"]) # throws error if the key is not exists
# print(customer.get("birthdate", "Jan 1 1980")) # returns 'None' or default value if we specify it