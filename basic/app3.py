# STRINGS
course = "Python for Beginners"
another_course = course[:]
# print(another_course)
#
# print(course[0]) # first charachter of string
# print(course[-1]) # last charachter of string
# print(course[0:3]) # substring
# print(course[0:]) # substring
# print(course[:5]) # first 5 charachter from beginning
# print(course[1:-1])
#
#
# # FORMATTED STRINGS
# first = 'John'
# last = 'Smith'
# msg = f'{first} [{last}] is a coder'
# print(msg)



# STRING METHODS
print(len(course))
print(course.upper())
print(course.lower())
print(course)
print(course.find('P')) # returns the index if it exists, otherwise returns -1
print(course.replace('Beginners', 'Absolute Beginners'))
print('Python' in course) # True
print('python' in course) # False
